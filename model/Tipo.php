<?php
/**
*
*/
require_once('app/Model.php');

class Tipo extends Model
{
    public $id;
    public $nombre;

    function __construct()
    {
        # code...
    }


    public static function readAll()
    {
        $db = Tipo::connect();
        $sql = "SELECT * FROM tipo;";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Tipo');
        return $stmt->fetchAll();
    }


    public static function findOne($idv)
    {
        $db = Product::connect();
        $sql = "SELECT * FROM tipo WHERE id = ?;";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(1,$idv);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Tipo');
        return $stmt->fetch();
    }

}
