<?php
/**
*
*/
require_once('app/Model.php');
require_once('model/Tipo.php');


class Product extends Model
{
    public $id;
    public $nombre;
    public $precio;
    public $fecha;
    public $id_tipo;

    function __construct()
    {
        # code...
    }


    public static function readAll()
    {
        $db = Product::connect();
        $sql = "SELECT * FROM producto;";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Product');
        return $stmt->fetchAll();
    }

    public static function store($idv,$name,$price,$dete,$type)
    {
        $db = Product::connect();
        $sql = "INSERT INTO producto VALUES(?,?,?,?,?)";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(1,$idv);
        $stmt->bindParam(2,$name);
        $stmt->bindParam(3,$price);
        $stmt->bindParam(4,$dete);
        $stmt->bindParam(5,$type);

        return $stmt->execute();
    }

    public static function findOne($idv)
    {
        $db = Product::connect();
        $sql = "SELECT * FROM producto WHERE id = ?;";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(1,$idv);
        $stmt->execute();
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Product');
        return $stmt->fetch();
    }

     public static function update($idv,$name,$price,$dete,$type)
    {
        $db = Product::connect();
        $sql = "UPDATE  producto SET  nombre=?,  precio=?,  fecha=?,  id_tipo=? WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->bindParam(5,$idv);
        $stmt->bindParam(1,$name);
        $stmt->bindParam(2,$price);
        $stmt->bindParam(3,$dete);
        $stmt->bindParam(4,$type);


        return $stmt->execute();
    }

    public static function typeName($tid)
    {
      $tipoNombre = Tipo::findOne($tid);
      return $tipoNombre->nombre;
    }



}
