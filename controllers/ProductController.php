<?php

/**
*
*/
require_once('model/Product.php');
require_once('model/Tipo.php');
class ProductController
{

    function __construct()
    {

    }

    public function index()
    {
        $products = Product::readAll();
         require ("views/product/index.php");
    }

    public function store(){


        $id = $_POST['id'];
        $nombre = $_POST['nombre'];
        $precio = $_POST['precio'];
        $fecha =date("Y-m-d", strtotime($_POST['fecha'])) ;
        $tipo = $_POST['tipo'];

        Product::store($id, $nombre, $precio,$fecha, $tipo);
        header('location: http://examen1.local/product/index');

    }

    public function create()
    {
         $tipos = Tipo::readAll();
         require ("views/product/store.php");
    }

    public function edit($id)
    {
      $product = Product::findOne($id);
      require ("views/product/edit.php");
    }


    public function update($id){

        $nombre = $_POST['nombre'];
        $precio = $_POST['precio'];
        $fecha = date("Y-m-d", strtotime($_POST['fecha']));
        $tipo = $_POST['tipo'];

        Product::update($id, $nombre,$precio,$fecha,$tipo);

        header('location: http://examen1.local/product/index');
    }

     public static function vista($id){
        $product = Product::findOne($id);
        $_SESSION['vista'][] = $product->nombre;
        header('location: http://examen1.local/product/index');
    }


}
