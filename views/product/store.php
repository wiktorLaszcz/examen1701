<?php require 'views/header.php'; ?>
<main>
    <form method="post" action="store">
        <label>Id</label>
        <input type="text" name="id">
        <label>Nombre</label>
        <input type="text" name="nombre">
        <label>Precio</label>
        <input type="text" name="precio">
        <label>Fecha</label>
        <input type="text" name="fecha">
        <label>Tipo</label>
        <select name=tipo>
            <?php foreach ($tipos as $tipo): ?>
                <option value="<?php echo $tipo->id ?>" > <?php echo $tipo->nombre ?> </option>
            <?php endforeach ?>
         </select>
        <input type="submit" name="guardar" value="Guardar">
    </form>
</main>
<?php require 'views/footer.php'; ?>
