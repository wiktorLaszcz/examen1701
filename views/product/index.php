<?php require 'views/header.php'; ?>
<main>
    <table>
        <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Precio</th>
            <th>Fecha</th>
            <th>Tipo</th>
            <th>Acciones</th>
        </tr>
        <?php foreach ($products as $product): ?>
        <tr>
             <td><?php echo $product->id ?></td>
            <td><?php echo $product->nombre ?></td>
            <td><?php echo $product->precio ?></td>
            <td><?php echo date("d-m-Y", strtotime($product->fecha)) ?></td>
            <td><?php echo $product->typeName($product->id_tipo) ?></td>
            <td><a href="http://examen1.local/product/edit/<?php echo $product->id?>">Editar</a>--
            <a href="http://examen1.local/product/vista/<?php echo $product->id?>">Ver</a>
            </td>
        </tr>
        <?php endforeach ?>
    </table>
    <a href="http://examen1.local/product/create">Nuevo</a>
</main>
<?php require 'views/footer.php'; ?>
