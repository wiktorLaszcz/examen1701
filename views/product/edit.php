<?php require 'views/header.php'; ?>
<main>
    <form method="post" action="../update/<?php echo $product->id ?>">
        <label>Nombre</label>
        <input type="text" name="nombre" value="<?php echo $product->nombre ?>">
        <label>Precio</label>
        <input type="text" name="precio" value="<?php echo $product->precio ?>">
        <label>Fecha</label>
        <input type="text" name="fecha" value="<?php echo date("d-m-Y", strtotime($product->fecha ))?>">
        <label>Tipo</label>
        <input type="text" name="tipo" value="<?php echo $product->id_tipo ?>">
        <input type="submit" name="guardar" value="Guardar">
    </form>
</main>
<?php require 'views/footer.php'; ?>
